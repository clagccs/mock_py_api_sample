from collections import namedtuple

import requests

from application import get_operating_system, sample_call, get_sample_call
from helper import dict_to_obj, mock_responses
from services import get_todos


def test_get_operating_system(mocker):
    mocker.patch('application.is_windows', return_value=True)
    assert get_operating_system() == 'Windows'


# sin mock
def test_request_response():
    # Send a request to the API server and store the response.
    response = get_todos()

    # Confirm that the request-response cycle completed successfully.
    assert response is not None


# con mock
def test_mocked_request_response(mocker):
    # hacemos mock directo de la llamada de la api

    api_response = {'ok': True}
    mocker.patch('requests.get', return_value=dict_to_obj(api_response))

    response = get_todos()
    print(response)
    assert response is not None
    assert response.ok is True


# con mock
def test_mocked_wrong_request_response(mocker):
    # hacemos mock directo de la llamada de la api

    api_response = {'error': 'some error message', 'ok': False}
    mocker.patch('requests.get', return_value=dict_to_obj(api_response))

    response = get_todos()
    print(response)
    assert response is None


# con mock usando otro formato
def test_mocked_wrong_request_response_other_format(mocker):
    # hacemos mock directo de la llamada de la api

    api_response = {'error': 'some error message', 'ok': False}
    m = mocker.patch('requests.get')
    m.return_value = dict_to_obj(api_response)

    response = get_todos()
    print(response)
    assert response is None


# otra forma, mocking directamente una url
def test_url(requests_mock):
    requests_mock.get('http://test.com', text='data')
    assert 'data' == requests.get('http://test.com').text


# caso que deba tener argumentos
def test_side_effect(mocker):
    m = mocker.patch('application.sample_call')
    m.side_effect = mock_responses(
        {
            'x': 42,
            'y': 33
        })

    result = get_sample_call('x')
    assert result is 42

