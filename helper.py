from collections import namedtuple


# requerida porque python es un asco y no podía acceder a response.ok porque era un dictionary
def dict_to_obj(dictionary, name="object_from_dict"):
    return namedtuple(name, dictionary.keys())(*dictionary.values())


def mock_responses(responses, default_response=None):
    return lambda input: responses[input] if input in responses else default_response
